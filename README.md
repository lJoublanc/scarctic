# SCARCTiC - SCala ARCtic Tick Collector

A tick-logger for [MAN/AHL's Arctic](https://github.com/manahl/arctic) driver written in the scala language.

Copyright (c) 2018 Dinosaur Merchant Bank Limited  

Author: Luciano Joublanc @lJoublanc

## Quickstart

You can include the library in your projects by adding this to your `build.sbt` file:

    resolvers += Resolver.bintrayRepo("dmbl","dinogroup")
    libraryDependencies += "com.dinogroup" %% "scarctic" % "0.4.2"

Here is an (untested) example:

```scala
import cats.effect._
import fs2.Stream
import scarctic._
import mongosaur.mongodb                  // For specifying connection URI.
import scodec.protocols.bson.implicits._  // Provides automatic (de)serialization

type Timestamp  = Long // Micros from Epoch

case class Tick(timestamp: Timestamp, price: Double, quantity: Int)

val data : Stream[IO,Tick] = ???

val program = 
  for { arctic <- scarctic[IO](mongodb.ConnectionString())
        lib <- arctic.library[TickStore]("test")
        res <- lib.write("VOD.LSE", data).foldMonoid
      } yield res

program.compile.last.unsafeRunSync match {
  case Some(mongodb.commands.Insert.Reply(n)) =>
    println("Records written: " + n)
}
```

Refer to the test directory for examples of simple CRUD operations.

## Features

  * Implements [tick-store](https://github.com/manahl/arctic#storage-engines) storage engine.

  * Allows streaming data in/out using [fs2](https://github.com/functional-streams-for-scala/fs2). The underlying chunk structure is optimized to avoid boxing, copying, and to keep the data packed column-wise when reading.

  * The [mongosaur](ljoublanc/mongosaur>) driver, by default, infers the columns from scala types and requests a projection of these. i.e. only required cols are fetched over the network.

## Documentation

The scaladoc can be generated with `sbt doc` under `target/scala-2.12/api/index.html` .
