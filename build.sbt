name := "scarctic"

description := "SCala ARCtic Tick Collector - A tick logger for Arctic"

organization := "com.dinogroup"

scalaVersion := "2.12.7"

// This sets the version. To override locally, create a version.sbt file.
enablePlugins(GitVersioning)

licenses += "MIT" -> url("http://opensource.org/licenses/MIT")

bintrayRepository := "dinogroup"

bintrayOrganization := Some("dmbl")

libraryDependencies ++= Seq(
  "com.dinogroup" %% "mongosaur" % "0.1.2",
  "com.dinogroup" %% "fs2-columns" % "0.1.2",
  "net.jpountz.lz4" % "lz4" % "1.3.0",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)

scalacOptions ++= Seq(
  "-language:implicitConversions",
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps",
  "-Ypartial-unification",
  "-Ywarn-value-discard",
  "-Ywarn-unused-import",
  "-deprecation"
  )

scalacOptions in (Compile, console) ~= {
  _.filterNot("-Ywarn-unused-import" == _)
   .filterNot("-Xlint" == _)
   .filterNot("-Xfatal-warnings" == _)
}
