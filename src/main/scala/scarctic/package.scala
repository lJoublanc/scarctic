import net.jpountz.lz4.{LZ4Compressor,LZ4FastDecompressor,LZ4Factory}

/** =SCARCTiC - SCala ARctic Tick Collector=
  * 
  * This library provides a scala interface to
  * [[https://gihub.com/manahl/arctic MAN/AHL's Arctic]] driver.  Refer to that
  * package for an overview of the database. The concepts there map closely to
  * those in this driver. A ''storage engine'' defines the interface for accessing
  * the database ''libraries''. Currently the available libraries are:
  *
  * <ul>
  * <li/> [[scarctic.TickStore.Library]]
  * </ul>
  *
  * Taking [[scarctic.TickStore.Library TickStore]] as an example, writing some stock
  * price data would be done like so:
  *
  * {{{
  *   import cats.effect._
  *   import fs2.Stream
  *   import scarctic._
  *   import mongosaur.mongodb                  // For specifying connection URI.
  *   import scodec.protocols.bson.implicits._  // Provides automatic (de)serialization
  *
  *   type Timestamp  = Long // Micros from Epoch
  *   
  *   case class Tick(timestamp: Timestamp, price: Double, quantity: Int)
  *
  *   val data : Stream[IO,Tick] = ???
  *
  *   val program = 
  *     for { arctic <- scarctic[IO](mongodb.ConnectionString())
  *           lib <- arctic.library[TickStore]("test")
  *           res <- lib.write("VOD.LSE", data).foldMonoid
  *         } yield res
  *   
  *   program.compile.last.unsafeRunSync match {
  *     case Some(mongodb.commands.Insert.Reply(n)) =>
  *       println("Records written: " + n)
  *   }
  * }}}
  * 
  * Reading it back would conversely be
  *
  * {{{ 
  *   import Interval._
  *   lib.read[Tick]("VOD.LSE", start = Unbounded)) 
  * }}}
  *
  * @author Luciano Joublanc
  */
package object scarctic {
  /** The default compressor, used by most stores. */
  implicit lazy val compressor: LZ4Compressor =
    LZ4Factory.fastestInstance.highCompressor()

  /** The default decompressor, used by most stores. */
  implicit lazy val decompressor: LZ4FastDecompressor =
    LZ4Factory.fastestInstance.fastDecompressor()
}
