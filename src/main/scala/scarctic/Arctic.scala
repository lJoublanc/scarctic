package scarctic

import cats.effect._
import fs2.Stream
import mongosaur.mongodb
import mongosaur.mongodb.commands.Command

/** Represents a logical connection to the Arctic DB, and acts as a factory to
 * create/access libraries. Your application will typically have only a single
 * shared `Arctic` instance. This handles graceful release of resources on
 * shutdown.
 */
trait Arctic[F[_]] {
  /** Create a library context.
    * When evaluated, this will check that the library exist and has been initialized
    * correctly. If not, this will be done for you.
    *
    * @tparam S The store type. This parameter is not inferred and must be passed in.
    * @param name The name of the library.
    * @return A singleton stream you can `flatMap` over to access the library.
    */
  def library[S[f[_]] <: Store[f]](name: String)(implicit store: S[F]): Stream[F,store.Library]
}

object Arctic {

  protected[scarctic] val DB_PREFIX = "arctic"

  protected[scarctic] val METADATA_COLL = "ARCTIC"

  protected[scarctic] val METADATA_DOC_ID = "ARCTIC_META"

  /** Not be confused with store-specific metadata, this is always stored 
    * in `METADATA_COLL`, with `_id = METADATA_DOC_ID`.
    */
  /*sealed abstract*/ case class Metadata(
    _id: String = "ARCTIC_META",
    /** The store type. */
    TYPE: String,
    /** Max size of the library, in bytes */
    QUOTA: Long = 10 * 1024 * 1024 * 1024
  )

  /*
  object Metadata {
    def apply(quotaBytes: Option[Long], libraryType: String): Metadata = 
      new Metadata(quotaBytes getOrElse 0L, libraryType) {}
  }*/

  /** Default constructor.
    * @tparam The effect type, must be passed explicitly.
    * @param connectionString The URI of the mongodb server hosting this arctic instance.
    */
  def apply[F[_] : ConcurrentEffect : Timer](connectionString: mongodb.ConnectionString): Stream[F,Arctic[F]] =
    mongodb.Client[F](connectionString) map { client =>
      new Arctic[F] {
        def library[S[f[_]] <: Store[f]](name: String)(implicit store: S[F]): Stream[F,store.Library] = {
          import scodec.protocols.bson.implicits._
          case class QueryMeta(_id: String)
          val meta = Metadata(TYPE = store.name) //TODO: set initial quota, once mechanism is implemented.

          def checkMetaExistsOfRightType(metaColl: mongodb.Collection[F]): Stream[F,Command.Ok] = 
            metaColl
              .find[Metadata](QueryMeta(_id = METADATA_DOC_ID))
              .last
              .flatMap {
                case None =>
                  metaColl insert meta
                case Some(meta) =>
                  if (meta.TYPE == store.name) Stream(Command.Ok)
                  else Stream raiseError StoreTypeMismatch(name,store.name,meta.TYPE)
              }

          if (name.startsWith(DB_PREFIX))
            Stream raiseError new Exception(
              s"Prefixing library name with `$DB_PREFIX` is ambiguous. Please don't do this."
            )
          else name split '.' match {
            case Array(dbsuffix,dataCollName) =>
              val db = client.database(DB_PREFIX + "_" + dbsuffix)
              val metaColl = db.collection(dataCollName + "." + METADATA_COLL)
              val dataColl = db.collection(dataCollName)
              checkMetaExistsOfRightType(metaColl) >> 
                store.initialize(name, db, metaColl, dataColl)
            case Array(dataCollName) =>
              val db = client.database(DB_PREFIX)
              val metaColl = db.collection(dataCollName + "." + METADATA_COLL)
              val dataColl = db.collection(dataCollName)
              checkMetaExistsOfRightType(metaColl) >> 
                store.initialize(name, db, metaColl, dataColl)
            case x =>
              Stream raiseError new Exception(
                s"Invalid library name `$name`; must be `a.b`"
              )
          }
        }
      }
    }

  case class StoreTypeMismatch(
    library: String,
    expectedStore: String,
    gotStore: String
  ) extends Exception(
    s"Expected library $library to be of type $expectedStore but found $gotStore"
  )
}
