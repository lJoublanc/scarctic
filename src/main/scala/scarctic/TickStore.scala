package scarctic

import cats.ApplicativeError
import codecs.TickStoreCodec
import fs2.Stream
import mongosaur.mongodb
import scodec.Codec
import Interval._
import java.time.Instant

/** Column-oriented tick store. Supports dynamic fields, chunks aren't
  * versioned. Designed for large continuously ticking data.
  * @param chunkSize The maximum number of ticks stored in a single chunk.
  *                  Chunks larger than this will be split.
  * @see [[https://github.com/manahl/arctic/arctic/tickstore/tickstore.py]]. 
  */
class TickStore[F[_]](val chunkSize: Int = 100000)(implicit F: ApplicativeError[F,Throwable]) extends Store[F] {
  import TickStore.Symbol

  protected val TICK_STORE_TYPE = "TickStoreV3"
  
  def name = TICK_STORE_TYPE

  type Library = TickStore.Library[F]

  /** Initializes the library. Creates collection and indices if they do not exist yet. */
  protected[scarctic] def initialize(
      library: String,
      db: mongodb.Database[F],
      metaColl: mongodb.Collection[F],
      dataColl: mongodb.Collection[F]
  ): Stream[F,Library] = {
    import mongodb.commands._
    import mongodb.commands.CreateIndexes._
    import scodec.protocols.bson.implicits._
    import shapeless.HNil

    case class BySymbolAndStart(sy: Int, s: Int)
    case class ByStart(s: Int)

    val indices = 
      LinearIndex("bySymbolAndStart", BySymbolAndStart(ascending,ascending)) ::
      LinearIndex("byStart", ByStart(ascending)) :: 
      HNil

    dataColl command CreateIndexes(dataColl.name,indices) as new TickStore.Library[F] {

      def name: String = TICK_STORE_TYPE

      def symbols: Stream[F,Symbol] = 
        for { rep <- dataColl command Distinct[String](dataColl.name,"sy") 
              sym <- Stream emits rep.values }
          yield sym

      def write[V <: Product](symbol: Symbol, data: Stream[F,V], image: Boolean = false)(
          implicit tsc: TickStoreCodec[V])
          : Stream[F, mongodb.commands.Insert.Reply] = {
        import fs2._
        import scodec.codecs._
        import TickStore.OverlappingDataException
        import scodec.protocols.bson.BSONElement
        import TickStoreCodec.DateRange

        type D = TickStore.Symbol ~ Chunk[V] ~ Option[V]

        def go(s: Stream[F,V], first: Option[V]): Pull[F,D,Unit] =
          s.pull.unconsLimit(chunkSize) flatMap {
            case Some((chunk, tail)) =>
              Pull.output1(symbol ~ chunk ~ first) >> go(tail, chunk.last)
            case None =>
              Pull.done
          }

        /* Stream of documents to insert. */
        val docs = if (image)
          data.pull.uncons1.flatMap {
            case Some((im,tail)) =>
              go(tail,Some(im))
            case None =>
              Pull.done
          }.stream
        else 
          go(data,None).stream

        /* Possibly raises [[OverlappingDataException]] */
        def assertOverlap(start: Instant, end: Instant): Stream[F,Nothing] = {
          import scodec.protocols.bson.implicits._ //TODO: for some reason, this has to be local; importing this in the closure doesn't work.
          import Find.Reply
          import Find.Reply.Cursor
          case class FirstDoc(s: Instant, e: Instant)
          case class Sort(s: Int)
          case class Project(_id: Int, s: Int, e: Int)                  

          implicit val dtcmd = Command[Find[DateRange,Sort,Project],Reply[FirstDoc]]
          implicit val dateRangeElem: BSONElement[DateRange] = 
            scodec.protocols.bson.document[DateRange,Codec](DateRange.encoder.encodeOnly)
          dataColl.command(
            Find(
              find = dataColl.fullyQualifiedName,
              filter = Some(DateRange(symbol,(Before(end),Unbounded))),
              projection = Some(Project(_id = 0, s = 1, e = 1)),
              sort = Some(Sort(s = 1)),
              limit = Some(1),
            )
          )
          .attempt //FIXME: see mongosaur/#19 - order of reply elements.
          .flatMap {  
            //ok; exeception is because reply to `find` re-orders elements when result set is empty, and codec fails.
            case Left(mongosaur.mongodb.CodecException(_)) => 
              Stream.empty
            case Right(Reply(Cursor(List(FirstDoc(firstStart,firstEnd)),_,_),_)) =>
              if (firstEnd isAfter start)
                Stream raiseError[F] OverlappingDataException(firstStart,firstEnd,start,end)
              else
                Stream.empty
          }
        }

        docs flatMap { case doc @ (_ ~ chunk ~ _) =>
          val start = Instant ofEpochMilli tsc.getTimestamp(chunk.head.get)
          val end = Instant ofEpochMilli tsc.getTimestamp(chunk.last.get)
          import scodec.protocols.bson.implicits._
          implicit val v: BSONElement[D] =
            scodec.protocols.bson.document[D,Codec](tsc.codec)
          assertOverlap(start,end).covaryOutput[Insert.Reply] ++ 
            dataColl.insert[cats.Id,D](doc)
        }
      }

      def read[V <: Product](symbol: Symbol, start: Interval, end: Interval = Unbounded, image: Boolean = false)(implicit tsc: TickStoreCodec[V])
          : Stream[F,V] = {
        import fs2._
        import scodec.codecs.{~,ValueEnrichedWithTuplingSupport}
        import TickStoreCodec.DateRange

        case class SymbolMismatch(expected: Symbol, got: Symbol) 
        extends Exception(s"Symbol mismatch: expected $expected, got $got")

        val docs = 
          dataColl.find[tsc.Repr](DateRange(symbol,(start,end)))(scodec.protocols.bson.bson(DateRange.encoder.encodeOnly),tsc.codec,tsc.fieldSelector)

        val imageAndChunks: Stream[F,Option[V] ~ Stream[F,V]] = 
          docs.pull.uncons1.flatMap { 
            case Some((sym ~ chunk ~ imo, tail)) => 
              if (sym == symbol) {
                val data = 
                  tail.flatMap{ case _ ~ c ~ _ => Stream chunk c }
                    .consChunk(chunk)
                Pull output1 (imo ~ data)
              }
              else 
                Pull raiseError[F] SymbolMismatch(symbol,sym)
            case None =>
              Pull.done
          }.stream
         
        imageAndChunks flatMap {
         case Some(imo) ~ data if image =>
           Stream(imo) ++ data
         case None ~ data if image =>
           Stream raiseError[F] new Exception("No image/snapshot available.")
         case _ ~ data if !image =>
           data
        }
      }

      def delete(symbols: Stream[F,Symbol]): Stream[F,Int] = ???

      def stats: Stream[F,Library.Statistics] = ???

    }
  }
}
      
object TickStore {

  type Symbol = String

  case class OverlappingDataException(firstStart: Instant, firstEnd: Instant, start: Instant, end: Instant) 
  extends Exception(s"Document already exists with start:$firstStart end:$firstEnd in the range of our start:$start end:$end")

  /** A tick-store library, supporting reading/writing `fs2.Stream`s that 
    * preserve the underlying chunk structure.
    * 
    * The key (`symbol`) is of type string, and stored data, typically
    * represented as a `case class`, must have a leading `Long` element as the
    * index. Snapshots (aka 'images') are supported also. Bitmasks are 
    * not supported, because not all primitive types admit `NaN` values.
    * 
    * One difference to the official python driver is that when reading data
    * out, integer types are not automatically converted into floats.
    *
    * This library will attempt to conserve the underlying `fs2.Stream` chunk
    * structure in it's operations.
    */
  trait Library[F[_]] extends scarctic.Library[F] {

    /** Return the list of stored symbols. */
    def symbols: Stream[F,Symbol]

    /** Write a stream to the tick-store. 
      *
      * The underlying `fs2.Stream` chunk structure is preserved when writing
      * documents, however a minimum `TickStore.chunkSize` is enforced.
      *
      * For each chunk compressed and inserted, resulting stream will return
      * an element.
      *
      * @tparam V A case class with a leading (index) `Long` element.
      * @param symbol The primary key.
      * @param data A stream of `V`.
      * @param image If `true`, the first element in `data` will be considered 
      *              to be the last known snapshot value before `data.head`.
      * @return `mongosaur.mongodb.commands.Insert.Reply`, a `cats.Semigroup`, 
      *         '''for every chunk inserted.'''.
      */
    def write[V <: Product : TickStoreCodec](symbol: Symbol, data: Stream[F,V], image: Boolean = false)
        : Stream[F, mongodb.commands.Insert.Reply]

    /** Read data from the tick-store. The resulting `fs2.Stream` will reflect the
      * underlying chunk structure in the database, and will be backed by
      * `fs2.Columns`, which keeps data vectorised and unboxed.
      *
      * The resulting stream can be accessed column-wise using the `fs2-columns` 
      * library (included).
      * @example
      * {{{
      *   import fs2.Columns
      *
      *   case class Tick(timestamp: Long, price: Double)
      * 
      *   val `Columns[Tick]` = Columns[Tick]   // create extractor for columns
      *
      *   // Calculate average price in each chunk
      *   lib.read[Tick]("VOD.LSE", start = Unbounded).mapChunks{
      *     case `Columns[Tick]`(timestamps :: prices :: HNil) =>
      *       prices.fold(_ + _) / prices.length
      *   }
      * }}}
      *
      *
      * @param symbol The primary key.
      * @param start The earliest data point to fetch.
      * @param end The last data point to fetch. Default to all data.
      * @param image If true, the last value prior to `start` will be cons'ed to the stream.
      * @return Stream of `V`s, with a snapshot prepended if `image = true`.
      */
    def read[V <: Product : TickStoreCodec](symbol: Symbol, start: Interval, end: Interval = Unbounded, image: Boolean = false):
      Stream[F,V]

    def delete(symbols: Stream[F,Symbol]): Stream[F,Int]
  }

  implicit def instance[F[_]](implicit F: ApplicativeError[F,Throwable]) = // : Store.Aux[F,TickStore] =
    new TickStore[F]

  protected val replyCodec = {
    import scodec.protocols.bson.implicits._
    Codec[mongodb.commands.Insert.Reply]
  }
}
