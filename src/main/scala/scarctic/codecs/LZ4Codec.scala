package scarctic.codecs

import java.nio.ByteBuffer
import scala.util.Try

import net.jpountz.lz4._
import scodec._
import scodec.bits._
import scodec.codecs.uint32L

/** Codec that deflates output bits of `valueCodec` with the LZ4 algorithm.  We
  * use the method adopted by the [[https://github.com/steeve/python-lz4 python
  * arctic driver]], so this is not necessarily portable.  Specifically, be
  * aware that the inflated byte size is prepended to the data, as this was 
  * implemented before LZ4 framing format was available.
  * [[https://github.com/steeve/python-lz4/blob/8ac9cf9df8fb8d51f40a3065fa538f8df1c8a62a/src/python-lz4.c#L100 See here]]
  * Note that the java LZ4 library interfaces are a bit messed up - there are
  * many with deprecated methods.  The only one that deals with bytebuffers is
  * LZ4FastDecompressor.
  * TODO: work on performance, specifically allocation of intermediate buffers.
  */
final class LZ4Codec[A](valueCodec: Codec[A])(
    implicit
    compressor: LZ4Compressor,
    decompressor: LZ4FastDecompressor) extends Codec[A] {

  def decode(bits : BitVector): Attempt[DecodeResult[A]] = 
    uint32L decode bits flatMap { 
      case DecodeResult(inflatedSizeBytes,rem) =>
        Attempt fromEither {
          Try {
            assert(inflatedSizeBytes.isValidInt)
            val outBuff = ByteBuffer allocate inflatedSizeBytes.toInt
            val bytesDecoded = decompressor.decompress(rem.bytes.toByteBuffer,0,outBuff,0,inflatedSizeBytes.toInt)
            DecodeResult(BitVector view outBuff,rem.bytes.drop(bytesDecoded).bits)
          }.toEither
          .left
          .map{
            case e: LZ4Exception => Err(e.getMessage)
          }
        } flatMap {
          case DecodeResult(bits,rem) =>
            valueCodec decode bits map { _ mapRemainder(_ ++ rem) }
        }
    }

  def encode(value: A) : Attempt[BitVector] = 
    for { inflatedBits <- valueCodec encode value
          sizeBits <- uint32L encode inflatedBits.bytes.size
          deflatedBits = BitVector view (compressor compress inflatedBits.toByteArray)
    } yield sizeBits ++ deflatedBits

  /** Beware that the worst case size bound can be ''larger'' than the original
    * data. Typically this is not the case.
    * Adapted from [[https://github.com/lz4/lz4-java/blob/7a162150b33f65e6b7e2390b2f38d8118e9cbe3e/src/java/net/jpountz/lz4/LZ4Utils.java#L32]] */
  def sizeBound: SizeBound = SizeBound(
    valueCodec.sizeBound.lowerBound + 16L,
    valueCodec.sizeBound.upperBound.map(l => l + l/255L + 16L)
  )

}
