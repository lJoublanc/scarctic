package scarctic.codecs

import java.time.Instant
import scala.annotation.implicitNotFound

import cats.instances.option._
import cats.syntax.apply._
import fs2._
import net.jpountz.lz4.{LZ4Compressor,LZ4FastDecompressor}
import scarctic.TickStore
import scarctic.Interval._
import scodec._
import scodec.bits._
import scodec.codecs.{~,ValueEnrichedWithTuplingSupport,constant}
import scodec.protocols.bson._
import shapeless._
import shapeless.labelled._
import shapeless.ops.hlist.{Mapped,IsHCons,LeftFolder}
import shapeless.ops.record.Keys
import mongosaur.Projection


/** A codec instance for TickStore V3.
  * @see [[scarctic.TickStore#Library]]
  */
@implicitNotFound("Unable to derive a codec for $A. Ensure the lead (index) element is of type `Long`, and that each member has a corresponding `NumpyChunkCodec[A]` and `BSONCodec[A]` in scope.")
trait TickStoreCodec[A] {

  /** The schema/document that will be written. See `tickstore.py` for example. */
  type Repr = TickStore.Symbol ~ Chunk[A] ~ Option[A]

  def codec: Codec[Repr]

  /** Used to limit the fields that are requested in e.g. a `find`. */
  implicit def fieldSelector: Projection[Repr]

  /** Returns the head element of `A`, as an `Instant`. */
  def getTimestamp(a: A): Long
}

object TickStoreCodec {

  type ID = Witness.`'_id`.T
  type SYMBOL = Witness.`'sy`.T
  type INDEX = Witness.`'i`.T
  type START = Witness.`'s`.T
  type END = Witness.`'e`.T
  @deprecated("v?","START_SEQ is not used in python API.") 
  type START_SEQ = Witness.`'sS`.T
  @deprecated("v?","END_SEQ is not used in python API.")
  type END_SEQ = Witness.`'eS`.T
  @deprecated("v?","SEGMENT is not used in python API.")
  type SEGMENT = Witness.`'se`.T
  type SHA = Witness.`'sh`.T
  type IMAGE_DOC = Witness.`'im`.T
  type IMAGE = Witness.`'i`.T

  type COLUMNS = Witness.`'cs`.T
  type DATA = Witness.`'d`.T
  type DTYPE = Witness.`'t`.T
  type IMAGE_TIME = Witness.`'t`.T
  type ROWMASK = Witness.`'m`.T

  type COUNT = Witness.`'c`.T
  type VERSION = Witness.`'v`.T

  type META = Witness.`'md`.T

  val CHUNK_VERSION_NUMBER = 3

  case class DateRange(sy: TickStore.Symbol, range: (Interval,Interval))

  object DateRange {
    /** Note this is unsealed to allow embedding in BSON.
      * @see `_daterange.py:DateRange` */
    protected[scarctic] val encoder = new Encoder[DateRange] {
      type `>` = Witness.`"$gt"`.T
      type `>=` = Witness.`"$gte"`.T
      type `<` = Witness.`"$lt"`.T
      type `<=` = Witness.`"$lte"`.T

      def encode(daterange: DateRange): Attempt[BitVector] =
        daterange match {
          case DateRange(sym, Unbounded ~ Unbounded) =>
            utf8[SYMBOL] encode sym
          case DateRange(sym, Closed(a) ~ Unbounded) =>
            utf8[SYMBOL] ~ document[START](utc[`>=`]) encode (sym ~ a)
          case DateRange(sym, Open(a) ~ Unbounded) =>
            utf8[SYMBOL] ~ document[START](utc[`>`]) encode (sym ~ a)
          case DateRange(sym, Unbounded ~ Closed(b)) =>
            utf8[SYMBOL] ~ document[END](utc[`<=`]) encode (sym ~ b)
          case DateRange(sym, Unbounded ~ Open(b)) =>
            utf8[SYMBOL] ~ document[END](utc[`<`]) encode (sym ~ b)
          case DateRange(sym, Closed(a) ~ Closed(b) )=> 
            utf8[SYMBOL] ~ document[START](utc[`>=`]) ~ document[END](utc[`<=`]) encode (sym ~ a ~ b)
          case DateRange(sym, Open(a) ~ Closed(b)) =>
            utf8[SYMBOL] ~ document[START](utc[`>`]) ~ document[END](utc[`<=`]) encode (sym ~ a ~ b)
          case DateRange(sym, Closed(a) ~ Open(b) )=> 
            utf8[SYMBOL] ~ document[START](utc[`>=`]) ~ document[END](utc[`<`]) encode (sym ~ a ~ b)
          case DateRange(sym, Open(a) ~ Open(b)) =>
            utf8[SYMBOL] ~ document[START](utc[`>`]) ~ document[END](utc[`<`]) encode (sym ~ a ~ b)
//Used internally for checking overlapping data.
          case DateRange(sym, Before(a) ~ Unbounded) =>
            utf8[SYMBOL] ~ document[START](utc[`<`]) encode (sym ~ a)
        }

      lazy val sizeBound: SizeBound = { 
        utf8[SYMBOL].sizeBound + 
        document[START](utc[`>`]).sizeBound +
        document[END](utc[`<`]).sizeBound
      }.atLeast
    }
  }

  /** @tparam L A shapeless record. */
  trait ColumnCodecs[L <: HList] {
    /** A codec of chunks aligned with `L` */
    type Repr <: Codec[_]

    def codec: Repr
  }

  object ColumnCodecs extends ColumnCodecsLowPrio {
    implicit def one[K <: Symbol, V](
        implicit
        witness: Witness.Aux[K],
        numpyChunkCodec: Codec[Chunk[V]] with NumpyDType,
        compressor: LZ4Compressor,
        decompressor: LZ4FastDecompressor
        ): Aux[FieldType[K,V] :: HNil, Codec[Chunk[V] :: HNil]] =
      new ColumnCodecs[FieldType[K,V] :: HNil] {

        type Repr = Codec[Chunk[V] :: HNil]

        val column: BSONElement[Chunk[V]] = lz4(numpyChunkCodec)

        val mask: BSONElement[BitVector] = lz4(codecs.bits)

        def codec: Repr = 
          document[K]{
            (column[DATA] ~ utf8[DTYPE] ~ mask[ROWMASK])
              .xmapc{ case chunk ~ dtype ~ mask => chunk } { //TODO: validate dtype & mask
                chunk => chunk ~ numpyChunkCodec.dtype ~ BitVector.high(chunk.size)
              }
          } :: codecs.provide(HNil: HNil)
      }
  }

  trait ColumnCodecsLowPrio {
    type Aux[L <: HList, M <: Codec[_]] = ColumnCodecs[L]{ type Repr = M }

    implicit def hcons[K <: Symbol, V, Rec <: HList, L <: HList](
        implicit
        witness: Witness.Aux[K],
        numpyChunkCodec: Codec[Chunk[V]] with NumpyDType,
        compressor: LZ4Compressor,
        decompressor: LZ4FastDecompressor,
        tail: Aux[Rec,Codec[L]]
        ): Aux[FieldType[K,V] :: Rec, Codec[Chunk[V] :: L]] =
      new ColumnCodecs[FieldType[K,V] :: Rec] {

        type Repr = Codec[Chunk[V] :: L]

        val column: BSONElement[Chunk[V]] = lz4(numpyChunkCodec)

        val mask: BSONElement[BitVector] = lz4(codecs.bits)

        def codec: Repr = 
          document[K]{
            (column[DATA] ~ utf8[DTYPE] ~ mask[ROWMASK])
              .xmapc{ case chunk ~ dtype ~ mask => chunk } { //TODO: validate dtype & mask
                chunk => chunk ~ numpyChunkCodec.dtype ~ BitVector.high(chunk.size)
              }
          } :: (tail.codec: Codec[L])
      }
  }

  
  /** @todo implement Time Unit.
    * @tparam A The generic product (i.e. case class) type.
    * @tparam L A shapeless record aligned with `A`.
    * @tparam LT The tail of `L` (i.e. excluding the index).
    */
  implicit def apply[
      A,
      L <: HList,
      LH,
      LT <: HList,
      HK <: Symbol,
      V <: HList,
      VT <: HList,
      DT <: HList,
      K <: HList
  ]( 
      implicit
      labels: LabelledGeneric.Aux[A,L],
      idxUnconsLab: IsHCons.Aux[L, LH, LT],
      idxIsLabelled: LH <:< FieldType[HK,Long],
      idxInvLabelled: FieldType[HK,Long] <:< LH,
      idxCons: (LH :: LT) =:= L,
      lazyImageCodec: Lazy[BSONCodec[LT]],
      generic: Generic.Aux[A,V],
      idxUncons: IsHCons.Aux[V, Long, VT],
      mappedValues: Mapped.Aux[V, Chunk, Chunk[Long] :: DT],
      columnCodecs: ColumnCodecs.Aux[LT,Codec[DT]],
      `Columns[A]`: Columns.Aux[A,Chunk[Long] :: DT],
      lz4Compressor: LZ4Compressor,
      lz4Decompressor: LZ4FastDecompressor,
      noId: NotContainsConstraint[L,ID],
      keys: Keys.Aux[L,K],
      folder: LeftFolder.Aux[K,Attempt[BitVector],FoldCols.type,Attempt[BitVector]]
  ): TickStoreCodec[A] = 
  new TickStoreCodec[A] {

    implicit val longToInstant: Long => Instant =
      Instant.ofEpochMilli(_)

    implicit val instantToLong: Instant => Long =
      _.toEpochMilli

    val imageCodec = lazyImageCodec.value

    val indexCodec = lz4(numpyLongChunk.xmap(δToIdx,idxToδ))

    private val c = 
      option[ID](objectId) ~ 
      utf8[SYMBOL] ~
      indexCodec[INDEX] ~
      option[IMAGE_DOC](document[LT ~ Instant,Codec](document[IMAGE](imageCodec) ~ utc[IMAGE_TIME])) ~
      document[COLUMNS](columnCodecs.codec: Codec[DT]) ~
      utc[START] ~
      utc[END] ~
      int32[VERSION]

    def codec: Codec[Repr] = new Codec[Repr] {

      def decode(bits: BitVector): Attempt[DecodeResult[Repr]] = 
        bson(c) decode bits map { decodeResult =>
          decodeResult map {
            case _ ~ sym ~ idxChunk ~ imo ~ colChunks ~ _ ~ _ ~ _ =>
              val im = imo.map { case values ~ timestamp =>
                val head: LH = field[HK][Long](instantToLong(timestamp))
                labels from (head :: values)
              }
              sym ~ `Columns[A]`(idxChunk :: colChunks) ~ im
          }
        }
      
      def encode(as: Repr): Attempt[BitVector] = {

        val sym ~ data ~ imageOpt = as

        val imo = imageOpt map { imo =>
          val l = labels to imo
          l.tail ~ longToInstant(l.head)
        }

        data match {
          case `Columns[A]`(idx :: values) =>
            Attempt fromOption (
              (idx.head map2 idx.last) { (start,end) =>
                c encode (None ~ sym ~ idx ~ imo ~ values ~ longToInstant(start) ~ longToInstant(end) ~ CHUNK_VERSION_NUMBER)
              },
              Err("No elements to encode") //TODO: should we allow image-only?
            ) flatten
          case rows => 
            encode(sym ~ `Columns[A]`.fromRows(rows, minChunkSize = 0) ~ imageOpt)
        }
      }

      def sizeBound: SizeBound = SizeBound.unknown
    }

    lazy val fieldSelector: Projection[Repr] = new Projection[Repr] {
      def codec: BSONCodec[Unit] = {
        constant {
          val values = 
            for { id <- int32[ID] encode 0
                  sy <- int32[SYMBOL] encode 1
                  i <- int32[INDEX] encode 1
                  im <- int32[IMAGE_DOC] encode 1
                  cs <- keys().foldLeft(Attempt successful BitVector.empty)(FoldCols)
                  s <- int32[START] encode 1
                  e <- int32[END] encode 1
                  v <- int32[VERSION] encode 1
            } yield id ++ sy ++ i ++ im ++ cs ++ s ++ e ++ v
          values.require
        }
      }.asInstanceOf[BSONCodec[Unit]]
    }

    def getTimestamp(a: A): Long = generic to a head
  }

  object FoldCols extends Poly2 {
    implicit def any[K](implicit wit: Witness.Aux[K]): 
        Case.Aux[Attempt[BitVector],K,Attempt[BitVector]] =
      at { (maybeAcc, col) =>
        import scala.Symbol
        val w = s"cs.${wit.value match { case Symbol(s) => s ; case s => s.toString }}"
        for { acc <- maybeAcc
              col <- int32(new Witness { type T = Symbol ; val value = Symbol(w) }) encode 1
            } yield acc ++ col
     }
  }

  //TODO: optimize; combine compression deltas into a bytebuffer?
  private def idxToδ(idx: Chunk[Long]): Chunk[Long] =
    Stream.chunk(idx)
      .zipWithPrevious
      .map {
         case (None,t0) => t0
         case (Some(tm),tn) => tn - tm
      }
      .toChunk
      
  private def δToIdx(deltas: Chunk[Long]): Chunk[Long] = 
    Stream chunk deltas scan1 (_ + _) toChunk

}
