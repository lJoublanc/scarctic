package scarctic.codecs

import fs2.Chunk
import fs2.Chunk._
import scodec.bits._
import scodec._
import java.nio

/** Default `fs2.Chunk` codec, available on all platforms. Returns a
  * `scala.Vector`-backed chunk, i.e. boxes primitives.
  * @param dtype The numpy `dtype` that `A` is encoded to.
  * @param value A codec for en/decoding a single `A`.
  */
class NumpyChunkCodec[A](override val dtype: String, val value: Codec[A])
extends Codec[Chunk[A]] with NumpyDType {

  def encode(chunk: Chunk[A]): Attempt[BitVector] =
    Encoder.encodeSeq(value)(chunk.toVector)

  def decode(bits: BitVector): Attempt[DecodeResult[Chunk[A]]] = 
    codecs.vector(value).asDecoder.map(Chunk.vector).decode(bits)

  def sizeBound: SizeBound = SizeBound.unknown
}

/** `fs2.Chunk` codec specialized for JVM primitives. Native JVM types will
  * translate to views backed by a `java.nio.Buffer`, other types will use the
  * passed `value` codec on individual elements.  Currently this only works on
  * Hotspot JVM.  
  * @see [[https://stackoverflow.com/questions/27492161/convert-floatbuffer-to-bytebuffer]]
  */
trait NumpyNativeChunkCodec[C <: Buffer[C,B,A],B <: nio.Buffer,A] extends
NumpyChunkCodec[A] { 

  protected val constructor: nio.ByteBuffer => C

  protected val extractor: Chunk[A] => Option[(sun.nio.ch.DirectBuffer, Int, Int)]

  abstract override def encode(chunk: Chunk[A]): Attempt[BitVector] =
    extractor(chunk)
      .map{
        case (b,o,l) =>
          assert(o == 0 && l == chunk.size) // FIXME: handle offset & length
          ByteVector view b.attachment.asInstanceOf[nio.ByteBuffer].order(nio.ByteOrder.LITTLE_ENDIAN) bits
      }
      .fold[Attempt[BitVector]](super.encode(chunk))(Attempt successful _)
      
  override def decode(bits: BitVector): Attempt[DecodeResult[Chunk[A]]] = 
    Attempt successful {
      DecodeResult(constructor(bits.toByteBuffer order nio.ByteOrder.LITTLE_ENDIAN), BitVector.empty)
    }
}
