package scarctic

import java.nio

import fs2.Chunk
import scodec.Codec
import scodec.{codecs => sc}
import scodec.protocols.bson
import net.jpountz.lz4._

package object codecs extends codecs.LowPrioCodecs {

  /** A codec that encodes/decodes NumPy. */
  type NumpyCodec[A] = Codec[A] with NumpyDType

  /** Marker for codecs that encode/decode NumPy.
    * This is to help with implicit resolution.
    */
  trait NumpyDType { self : Codec[_] => 
    /** The NumPy [[https://docs.scipy.org/doc/numpy/reference/arrays.dtypes.html dtype]]. */
    def dtype: String
  }
  
  /** BSONElement that deflates bits after encoding using the LZ4 algorithm 
    * and inflates before decoding, storing a BSON binary field.
    */
  def lz4[A](chunkCodec: Codec[A])(
      implicit
      compressor: LZ4Compressor,
      decompressor: LZ4FastDecompressor): bson.BSONElement[A] = {
    val c = new LZ4Codec(chunkCodec)
    new bson.BSONElement(
      bson.binary.opcode,
      bson.binary.value.exmap(
        c decodeValue _.bits,
        c encode _ map (_.bytes)
      )
    )
  }

  /** Codec for `Chunk[Long]` with fast zero-copy views. */
  implicit def numpyLongChunk: NumpyChunkCodec[Long] = 
    new NumpyChunkCodec[Long] ("int64", sc.int64L) with NumpyNativeChunkCodec[Chunk.LongBuffer, nio.LongBuffer, Long] {
      override lazy val constructor = Chunk.LongBuffer view _.asLongBuffer
      override lazy val extractor = 
        ({ case Chunk.LongBuffer(b: sun.nio.ch.DirectBuffer,o,l) => (b,o,l) }:
           PartialFunction[Chunk[Long],(sun.nio.ch.DirectBuffer,Int,Int)]) lift
    }

  /** Codec for `Chunk[Int]` with fast zero-copy views. */
  implicit def numpyIntChunk: NumpyChunkCodec[Int] = 
    new NumpyChunkCodec[Int] ("int32", sc.int32L) with NumpyNativeChunkCodec[Chunk.IntBuffer, nio.IntBuffer, Int] {
      override lazy val constructor = Chunk.IntBuffer view _.asIntBuffer
      override lazy val extractor = 
        ({ case Chunk.IntBuffer(b: sun.nio.ch.DirectBuffer,o,l) => (b,o,l) }:
           PartialFunction[Chunk[Int],(sun.nio.ch.DirectBuffer,Int,Int)]) lift
    }

  /** Codec for `Chunk[Float]` with fast zero-copy views. */
  implicit def numpyFloatChunk: NumpyChunkCodec[Float] = 
    new NumpyChunkCodec[Float] ("float32", sc.floatL) with NumpyNativeChunkCodec[Chunk.FloatBuffer, nio.FloatBuffer, Float] {
      override lazy val constructor = Chunk.FloatBuffer view _.asFloatBuffer
      override lazy val extractor = 
        ({ case Chunk.FloatBuffer(b: sun.nio.ch.DirectBuffer,o,l) => (b,o,l) }:
           PartialFunction[Chunk[Float],(sun.nio.ch.DirectBuffer,Int,Int)]) lift
    }

  /** Codec for `Chunk[Double]` with fast zero-copy views. */
  implicit def numpyDoubleChunk: NumpyChunkCodec[Double] = 
    new NumpyChunkCodec[Double] ("float64", sc.doubleL) with NumpyNativeChunkCodec[Chunk.DoubleBuffer, nio.DoubleBuffer, Double] {
      override lazy val constructor = Chunk.DoubleBuffer view _.asDoubleBuffer
      override lazy val extractor = 
        ({ case Chunk.DoubleBuffer(b: sun.nio.ch.DirectBuffer,o,l) => (b,o,l) }:
           PartialFunction[Chunk[Double],(sun.nio.ch.DirectBuffer,Int,Int)]) lift
    }

}

package codecs {
  protected[codecs] trait LowPrioCodecs {
    implicit def numpyBoxedChunk[A](implicit value: NumpyCodec[A]): Codec[Chunk[A]] = 
      sc.vector(value).xmap(Chunk.vector,_.toVector)
  }
}
