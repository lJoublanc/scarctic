package scarctic

import fs2.Stream
import mongosaur.mongodb

/** A storage-engine; essentially a factory for libraries of type
  * [[Store#Library]].  Abstract type `Library` is overridden to declare the
  * interface for this particular store/library.
  */
trait Store[F[_]] {

  /** The type of [[scarctic.Library]] managed by this store and returned by [[Arctic#library]] */
  type Library <: scarctic.Library[F]

  /** Name of the storage engine.
    * This is persisted in the library metadata to ensure you don't accidentally
    * use the wrong store.
    */
  def name: String

  /** Create db, indices and metadata collection if they don't exist already. 
    *
    * A call to 'initialize_library("a.b") will create:
    * 1) a database called "arctic_a"
    * 2) a collection within this called "b" that has the individual chunks.
    * 3) a collection "b.ARCTIC" with a record { _id : ARCTIC_META, QUOTA: 1234L, TYPE : TickStoreV3}
    *
    * A call to 'initialize_library("a") will create:
    * 1) a database called "arctic"
    * 2) a collection within this called "b"
    * 3) as above, #3.
    *
    * @param library The library name.
    * @param meta Initial metadata to insert, if it doesn't exist yet.
    * @return Singleton stream with the initialized library. `flatMap` to access it.
    */
  protected[scarctic] def initialize(
    library: String,
    db: mongodb.Database[F],
    metaColl: mongodb.Collection[F],
    dataColl: mongodb.Collection[F]
  ): Stream[F,Library]
}
