package scarctic

import java.time.Instant

/** An enumeration used to determine whether intervals of timestamps are
  * closed/open/unbounded.
  */
sealed trait Interval

object Interval {
  type Interval = scarctic.Interval

  trait Bounded extends Interval { def toInstant: Instant }

  final case class Open(toInstant: Instant) extends Bounded

  final case class Closed(toInstant: Instant) extends Bounded

  final case object Unbounded extends Interval

  // Used internally by tickstore
  protected[scarctic] final case class Before(toInstant: Instant) extends Interval
}
