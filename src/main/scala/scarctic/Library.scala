package scarctic

import fs2.Stream

/** Minimal interface for a library, defined and extended in each individual
  * instance of the [[Store]] typeclass. Each library is made up of a number of
  * collections, including a special metadata collection.
  *
  * @param F The effect type.
  */
trait Library[F[_]]{

  /** The name of this library instance (not store). */
  def name: String

  /** Return statistics for the entire library such as quota etc.
    * TODO: not clear if this is specific per-library, but they all must 
    * implemented, as it's calle din python Arctic.initialize_library
    */
  def stats: Stream[F,Library.Statistics]

}

object Library {

  // See python driver Tickstore.stats TODO: Not clear if this is the same for all stores.
  case class Statistics(
    //dbstats: DbStats.Reply
    //chunks: Collstats.Reply
    totals: Statistics.Totals
  )

  object Statistics {
    case class Totals(
      /** The store size, in bytes. */
      size: Int,
      /** The number of documents (?) */
      count: Int
    )
  }
}
