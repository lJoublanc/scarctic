package scarctic.codecs

import org.scalatest._
import scodec.protocols.bson._
import scodec.bits.ByteVector
import scodec.Attempt.Successful
import scarctic.{compressor,decompressor}

class Codecs extends FlatSpec with Matchers {

  type Label = shapeless.Witness.`"33333"`.T

  "numpy codec" should "roundtrip" in {
    import java.nio
    import fs2.Chunk
    val codec = numpyLongChunk
    val ls = Array.fill(20)(-1L)
    val bits = codec encode Chunk.Longs(ls)
    val native = 
      ByteVector view {
        val b = nio.ByteBuffer.allocateDirect(ls.length * 8)
        b.asLongBuffer.put(ls)
        b
      }

    withClue("encode") {
      bits shouldEqual Successful(native.bits)
    }
    withClue("decode boxed") {
      codec decodeValue bits.require shouldEqual Successful(Chunk.Longs(ls))
    }
    withClue("decode zero-copy view") {
      codec decodeValue native.bits shouldEqual Successful(Chunk.Longs(ls))
    }
  }

  "lz4 codec" should "roundtrip" in {
    val codec = lz4(int32[Label]).apply[Label]
    val value = -1
    val bits = codec encode value 

    codec decodeValue bits.require shouldEqual Successful(value)
  }
}
