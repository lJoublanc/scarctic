package scarctic

import java.nio

import org.scalatest._
import cats.effect._
import fs2._
import shapeless._
import mongosaur.mongodb

class CRUD extends FlatSpec with Matchers {

  implicit val timer= IO timer scala.concurrent.ExecutionContext.global
  implicit val cs = IO contextShift scala.concurrent.ExecutionContext.global
  implicit val E = cats.ApplicativeError[IO,Throwable]

  final val MAX = 3

  case class Tick(index: Long, value: Int, value2: Double)
  val indexArray = 0L until MAX.toLong toArray
  val columnOneArray = Array.fill(MAX)(-1)
  val columnTwoArray = Array.fill(MAX)(0.0)
  val index: Chunk[Long] = Chunk.LongBuffer(nio.LongBuffer.wrap(indexArray))
  val columnOne: Chunk[Int] = Chunk.IntBuffer(nio.IntBuffer.wrap(columnOneArray))
  val columnTwo: Chunk[Double] = Chunk.DoubleBuffer(nio.DoubleBuffer.wrap(columnTwoArray))
  val `Columns[Tick]` = Columns[Tick]
  val colChunk = `Columns[Tick]`(index :: columnOne :: columnTwo :: HNil)
  val rowChunk = Chunk.boxed((indexArray,columnOneArray,columnTwoArray).zipped.map(Tick).toArray)
  val cols: Stream[IO,Tick] = Stream chunk colChunk
  val rows: Stream[IO,Tick] = Stream chunk rowChunk

  "scarctic" should "write" in {
    {
      import scodec.protocols.bson.implicits._
      for { arctic <- Arctic[IO](mongodb.ConnectionString())
            lib <- arctic.library[TickStore]("test")
            rep1 <- lib.write("BBG00000000", cols).last
            _ = withClue("columns, image = false") {
              rep1 shouldEqual Some(mongodb.commands.Insert.Reply(1))
            }
            rep2 <- lib.write("BBG00000001", rows, image = true).last
            _ = withClue("rows, image = true") {
              rep2 shouldEqual Some(mongodb.commands.Insert.Reply(1))
            }
      } yield ()
    }.compile.drain.unsafeRunSync
  }

  it should "read" in {
    {
      import scodec.protocols.bson.implicits._
      import Interval._
      case class TwoVals(index: Long, value2: Double)
      for { arctic <- Arctic[IO](mongodb.ConnectionString())
            lib <- arctic.library[TickStore]("test")
            rep1 <- lib.read[Tick]("BBG00000000", start = Unbounded).chunks.fold(List[Chunk[Tick]]())( (as,a) => a :: as ).map(_.reverse)
            _ = withClue("image = false") {
              rep1 shouldEqual (colChunk :: Nil)
            }
            rep2 <- lib.read[Tick]("BBG00000001", start = Unbounded, image = true).chunks.fold(List[Chunk[Tick]]())( (as,a) => a :: as ).map(_.reverse)
            _ = withClue("image = true") {
              rep2 shouldEqual colChunk.splitAt(1).productIterator.toList
            }
            rep3 <- lib.read[TwoVals]("BBG00000000", start = Unbounded).chunks.fold(List[Chunk[TwoVals]]())( (as,a) => a :: as ).map(_.reverse)
            _ = withClue("with field selector") {
              rep3 shouldEqual List(Columns[TwoVals].apply(index :: columnTwo :: HNil))
            }
      } yield ()
    }.compile.drain.unsafeRunSync
  }
}
